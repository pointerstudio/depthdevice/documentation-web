$(document).ready(function() {
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
       $("body").removeClass("hideBody");
       $("body").addClass("mobile");

    }else if(('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0) && (testratio<0.7777777) ){
        $("body").removeClass("hideBody");
        $("body").addClass("mobile mobileOld");

    }else{
        $("body").removeClass("hideBody");
        $("body").addClass("desktop")
    }

    split();
    var randomator = setInterval(randomColor,1000);
    $(".jumpRotator svg").css("fill", colors[getRandomIntInclusive()]);
    // $("a").css("color", colors[getRandomIntInclusive()]);

    $(".desktop .buttons h2").hover(function(){
        $(this).css("color", colors[getRandomIntInclusive()])
    }, function(){
        $(this).css("color", "lightgrey")
    })

    $("li").each(function(i,e){
        if($(e).find('input[checked]').length > 0){
            $(e).prepend("<span class='green'>⬤ </span>")
        }else{
            $(e).prepend("<span class='red'>⬤ </span>")
        }
        $(e).find('> input').remove();
    })
});

$('.marked').each((i,e) => {
    const html = $(e).html();
    $(e).html(marked.parse(html));
});



function scrollTo(chapter){
    console.log("click");
    var doc = $(".container-text");
    var top = $(doc).find("."+chapter).offset().top + doc.scrollTop() - 10;
    $(doc).stop().animate({
        scrollTop: top
    }, 1000, function() {

    });
}

$(".buttons h2").each(function(i,e) {
    $(e).click(function(){
        var scrollTarget = $(e).attr("class");
        scrollTo(scrollTarget);
    })
})

$(".container-text").scroll(function(){
    $(".jumpRotator svg").css("transform","rotate("+$(".container-text").scrollTop()+"deg)")
});

function getRandomIntInclusive(min, max) {
  min = Math.ceil(0);
  max = Math.floor(2);
  return Math.floor(Math.random() * (max - min + 1) + min); //The maximum is inclusive and the minimum is inclusive
}

var colors = ["rgb(255,0,0)","rgb(0,255,0)","rgb(0,0,255)"]

function split(){
    $("h1").html(function(index, html) {
        return html.replace(/\w+/g, '<span class="word">$&</span>');
    });
    $(".word").html(function(index, html) {
        return html.replace(/\S/g, '<span class="letter">$&</span>');
    })
    randomColor();
}

function randomColor(){
    $(".letter").each(function (i,e) {
        $(e).css("color", colors[getRandomIntInclusive()])
    })
}

